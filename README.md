# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Lunatech assignment

### How do I get set up? ###

* Install Maven 
* Install Java 8
* Download the three csv files to the machine (airport,country,runways)
* Run the application using command `mvn spring-boot:run

Steps to run

    Build the project using `mvn clean package` skip the tests using `-DskipTests`
    Run the Project using command `mvn spring-boot:run -Dcsv.country=file:/PATH/TO/CSV -Dcsv.runway=file:/PATH/TO/CSV -Dcsv.airport=file:/PATH/TO/CSV`
	

### Known Issues ###

* Test cases of the size limit is failing because the limit is not supported directly by hibernate.
* Exception handling
