CREATE TABLE IF NOT EXISTS surface (
    id VARCHAR(20) PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS continent (
    id VARCHAR(15) PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS country (
    country_code VARCHAR(15) primary key,
    country_name VARCHAR(50),
    continent_id VARCHAR(15),
    wiki_link VARCHAR(100),
    keywords VARCHAR(500),
    FOREIGN KEY (continent_id)
        REFERENCES continent (id)
);
 
CREATE TABLE IF NOT EXISTS airport_type (
    id int primary key,
    airport_type varchar(40)
);

CREATE TABLE IF NOT EXISTS airport (
    id int primary key,
    airport_ident VARCHAR(20),
    airport_type_id int,
    airport_name VARCHAR(100),
    country_code VARCHAR(15),
    lat FLOAT,
    lng FLOAT,
    elevation FLOAT,
    wiki_link VARCHAR(100),
    keywords VARCHAR(500),
    FOREIGN KEY (country_code)
        REFERENCES country (country_code),
    FOREIGN KEY (airport_type_id)
        REFERENCES airport_type (id)
);

CREATE TABLE IF NOT EXISTS runways (
    id INT PRIMARY KEY,
    airport_id INT,
    length FLOAT,
    width FLOAT,
    surface_id VARCHAR(20),
    FOREIGN KEY (airport_id)
        REFERENCES airport (id),
    FOREIGN KEY (surface_id)
        REFERENCES surface (id)
);