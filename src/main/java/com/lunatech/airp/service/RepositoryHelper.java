/**
 * 
 */
package com.lunatech.airp.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lunatech.airp.model.Airport;
import com.lunatech.airp.model.AirportType;
import com.lunatech.airp.model.Continent;
import com.lunatech.airp.model.Country;
import com.lunatech.airp.model.Runway;
import com.lunatech.airp.model.Surface;
import com.lunatech.airp.model.csv.AirportCsv;
import com.lunatech.airp.model.csv.CountryCSV;
import com.lunatech.airp.model.csv.RunwayCsv;
import com.lunatech.airp.repository.AirportRepository;
import com.lunatech.airp.repository.AirportTypeRepository;
import com.lunatech.airp.repository.ContinentRepository;
import com.lunatech.airp.repository.CountryRepository;
import com.lunatech.airp.repository.RunwayRepository;
import com.lunatech.airp.repository.SurfaceRepository;

/**
 * @author karthik
 *
 */
@Component
public class RepositoryHelper {

	@Autowired
	private ContinentRepository continentRepository;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private AirportRepository airportRepository;
	@Autowired
	private AirportTypeRepository airportTypeRepository;
	@Autowired
	private SurfaceRepository surfaceRepository;
	@Autowired
	private RunwayRepository runwayRepository;
	
	
	public void addCSVDataToRepository(List<CountryCSV> countryValues, List<RunwayCsv> runwayValues,
			List<AirportCsv> airportValues) {
		List<Country> countries = countryValues.stream().map((csv) -> {
			return processCountry(continentRepository, countryRepository, csv);
		}).collect(Collectors.toList());

		countryRepository.save(countries);

		List<Airport> airports = airportValues.stream().map((csv) -> {
			return processAirport(countryRepository, airportRepository, airportTypeRepository, csv);
		}).collect(Collectors.toList());

		airportRepository.save(airports);

		List<Runway> runways = runwayValues.stream().map((csv) -> {
			return processRunway(airportRepository, runwayRepository, surfaceRepository, csv);
		}).collect(Collectors.toList());
		runwayRepository.save(runways);

	}

	private Runway processRunway(AirportRepository airportRepository, RunwayRepository runwayRepository,
			SurfaceRepository surfaceRepository, RunwayCsv csv) {
		Runway runway = new Runway();
		runway.setId(csv.getId());
		runway.setAirport(airportRepository.findOne(csv.getAirport_ref()));
		runway.setLength(csv.getLength_ft());
		runway.setWidth(csv.getWidth_ft());
		runway.setSurface(getSurface(surfaceRepository, csv.getSurface()));
		return runway;
	}

	private Airport processAirport(CountryRepository countryRepository, AirportRepository airportRepository,
			AirportTypeRepository airportTypeRepository, AirportCsv csv) {
		Airport airport = new Airport();
		airport.setId(csv.getId());
		airport.setAirportIdent(csv.getIdent());
		AirportType airportType = getAirportType(csv.getType(), airportTypeRepository);
		airport.setAirportType(airportType);
		airport.setAirportName(csv.getName());
		airport.setLat(csv.getLatitude_deg());
		airport.setLng(csv.getLongitude_deg());
		airport.setElevation(csv.getElevation_ft());
		airport.setCountry(getCountry(csv.getIso_country(), countryRepository));
		airport.setWikiLink(csv.getWikipedia_link());
		airport.setKeywords(csv.getKeywords());
		return airport;
	}

	private Country processCountry(ContinentRepository continentRepository, CountryRepository countryRepository,
			CountryCSV csv) {
		Country country = new Country();
		country.setId(csv.getId());
		country.setCountryCode(csv.getCode());
		country.setCountryName(csv.getName());

		country.setContinent(getContinent(continentRepository, csv.getContinent()));

		country.setWikiLink(csv.getWikipediaLink());
		country.setKeywords(csv.getKeywords());
		return country;
	}

	private Surface getSurface(SurfaceRepository surfaceRepository, String name) {
		Surface surface = surfaceRepository.findByName(name);
		if (surface == null) {
			surface = new Surface();
			surface.setName(name);
			surface = surfaceRepository.save(surface);
		}
		return surface;
	}

	private Country getCountry(String string, CountryRepository countryRepository) {
		Country country = countryRepository.findByCountryCode(string);
		return country;
	}

	private AirportType getAirportType(String string, AirportTypeRepository airportTypeRepository) {
		AirportType type = airportTypeRepository.findByTypeName(string);
		if (type == null) {
			type = new AirportType();
			type.setTypeName(string);
			airportTypeRepository.save(type);
		}
		return type;
	}

	private Continent getContinent(ContinentRepository continentRepository, String continentCode) {
		Continent continent = continentRepository.findByName(continentCode);
		if (continent == null) {
			continent = new Continent();
			continent.setName(continentCode);
			continentRepository.save(continent);
		}
		return continent;
	}
}
