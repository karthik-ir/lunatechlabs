/**
 * 
 */
package com.lunatech.airp.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lunatech.airp.model.csv.AirportCsv;
import com.lunatech.airp.model.csv.CountryCSV;
import com.lunatech.airp.model.csv.RunwayCsv;
import com.opencsv.bean.CsvToBeanBuilder;

/**
 * @author karthik
 *
 */
@Component
public class CSVProcessor {

	@Autowired
	private ExecutorService executorService;

	@Autowired
	private RepositoryHelper repositoryHelper;

	public File init(File countries, File airports, File runways) throws InterruptedException, ExecutionException {

		CountDownLatch latch = new CountDownLatch(3);
		Future<List<CountryCSV>> countryFuture = executorService.submit(() -> {
			try {
				List<CountryCSV> countryValues = convertCsvToBean(countries, CountryCSV.class);
				latch.countDown();
				return countryValues;
			} catch (IOException e) {
				throw new RuntimeException("Error processing Country CSV");
			}
		});

		Future<List<RunwayCsv>> runwayFuture = executorService.submit(() -> {
			try {
				List<RunwayCsv> runwayValues = convertCsvToBean(runways, RunwayCsv.class);
				latch.countDown();
				return runwayValues;
			} catch (IOException e) {
				throw new RuntimeException("Error processing runways CSV");
			}
		});

		Future<List<AirportCsv>> airportFuture = executorService.submit(() -> {
			try {
				List<AirportCsv> airportValues = convertCsvToBean(airports, AirportCsv.class);
				latch.countDown();
				return airportValues;
			} catch (IOException e) {
				throw new RuntimeException("Error processing AirportCsv CSV");
			}
		});

		latch.await();
		repositoryHelper.addCSVDataToRepository(countryFuture.get(), runwayFuture.get(), airportFuture.get());
		return null;

	}

	private <T> List<T> convertCsvToBean(File fileName, Class<T> t) throws IOException {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(FileUtils.readFileToByteArray(fileName))));
		CsvToBeanBuilder<T> csvToBeanBuilder = new CsvToBeanBuilder<T>(reader);
		if (t.getName().contains("RunwayCsv"))
			csvToBeanBuilder.withSkipLines(1);

		return csvToBeanBuilder.withType(t).build().parse();
	}
}
