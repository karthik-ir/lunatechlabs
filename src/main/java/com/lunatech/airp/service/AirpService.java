/**
 * 
 */
package com.lunatech.airp.service;

import org.jvnet.hk2.annotations.Service;
import org.springframework.data.domain.Pageable;

import com.lunatech.airp.model.rest.PagedAirportResponse;
import com.lunatech.airp.model.rest.Report;

/**
 * @author karthik
 *
 */
@Service
public interface AirpService {

	public PagedAirportResponse getAirportsByCountry(String country, Pageable pageable);

	public Report getReport();
}
