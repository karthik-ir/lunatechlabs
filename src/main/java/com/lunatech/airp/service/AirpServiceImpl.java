/**
 * 
 */
package com.lunatech.airp.service;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.lunatech.airp.model.Airport;
import com.lunatech.airp.model.rest.AirportResponse;
import com.lunatech.airp.model.rest.CountryAirportMapping;
import com.lunatech.airp.model.rest.CountryRunwayTypeMapping;
import com.lunatech.airp.model.rest.PagedAirportResponse;
import com.lunatech.airp.model.rest.Report;
import com.lunatech.airp.model.rest.RunwayResponse;
import com.lunatech.airp.repository.AirportRepository;

/**
 * @author karthik
 *
 */
@Component
public class AirpServiceImpl implements AirpService {
	@Autowired
	private AirportRepository airportRepository;

	@Override
	@Transactional
	public PagedAirportResponse getAirportsByCountry(String country, Pageable pageable) {
		Page<Airport> airports = airportRepository.findByCountryCountryNameContains(country, pageable);
		PagedAirportResponse pagedBlogs = new PagedAirportResponse();
		pagedBlogs.setTotalElements(airports.getTotalElements());
		pagedBlogs.setTotalPages(airports.getTotalPages());
		List<AirportResponse> airportResponses = convertAirportToAirportResponse(airports.getContent());
		pagedBlogs.setAirports(airportResponses);
		return pagedBlogs;
	}

	List<AirportResponse> convertAirportToAirportResponse(List<Airport> airports) {
		return airports.stream().map(airport -> {
			AirportResponse airportResponse = new AirportResponse();
			try {
				PropertyUtils.copyProperties(airportResponse, airport);
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			airportResponse.setRunways(airport.getRunways().stream().map(runway -> {
				RunwayResponse runwayResponse = new RunwayResponse();
				try {
					PropertyUtils.copyProperties(runwayResponse, runway);
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return runwayResponse;
			}).collect(Collectors.toList()));
			return airportResponse;
		}).collect(Collectors.toList());
	}

	@Override
	public Report getReport() {
		// top 10
		List<CountryAirportMapping> findBottom10Countries = airportRepository.findTop10AirportsAsc();
		List<CountryAirportMapping> findTop10Countries = airportRepository.findTop10AirportsDesc();
		List<CountryRunwayTypeMapping> runwayTypes = airportRepository.findrunwayTypesPerCountry();
		Map<String, Set<String>> countriesToRunwayMapping = new HashMap<>();
		runwayTypes.stream().forEach(x -> {
			if (countriesToRunwayMapping.containsKey(x.getCountryName())) {
				countriesToRunwayMapping.get(x.getCountryName()).add(x.getRunwayType());
			} else {
				HashSet<String> hashSet = new HashSet<>();
				hashSet.add(x.getRunwayType());
				countriesToRunwayMapping.put(x.getCountryName(), hashSet);
			}
		});
		Report report = new Report();

		report.setHighestNumberOfAirports(findTop10Countries);
		report.setLowestNumberOfAirports(findBottom10Countries);
		report.setRunwayTypes(countriesToRunwayMapping);
		return report;
	}

}
