/**
 * 
 */
package com.lunatech.airp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lunatech.airp.model.rest.PagedAirportResponse;
import com.lunatech.airp.model.rest.Report;
import com.lunatech.airp.service.AirpService;

/**
 * @author karthik
 *
 */
@RestController(value = "/")
public class AirController {

	@Autowired
	AirpService airpService;

	@RequestMapping(produces="application/json",method=RequestMethod.GET,value="/query")
	public ResponseEntity<PagedAirportResponse> query(@RequestParam(name = "country") String countryName,Pageable pageable) {
		PagedAirportResponse airportsByCountry = airpService.getAirportsByCountry(countryName,pageable);
		return new ResponseEntity<PagedAirportResponse>(airportsByCountry, HttpStatus.OK);
	}

	@RequestMapping(produces="application/json",method=RequestMethod.GET,value="/report")
	public ResponseEntity<Report> reports() {
		Report report = airpService.getReport();
		return new ResponseEntity<Report>(report, HttpStatus.OK);
	}

}
