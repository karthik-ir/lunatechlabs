package com.lunatech.airp.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the runways database table.
 * 
 */
@Entity
@Table(name = "runways")
@NamedQuery(name = "Runway.findAll", query = "SELECT r FROM Runway r")
public class Runway implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Float length;

	private Float width;

	// bi-directional many-to-one association to Airport
	@ManyToOne
	private Airport airport;

	// bi-directional many-to-one association to Surface
	@ManyToOne
	private Surface surface;

	public Runway() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getLength() {
		return this.length;
	}

	public void setLength(Float length) {
		this.length = length;
	}

	public Float getWidth() {
		return this.width;
	}

	public void setWidth(Float width) {
		this.width = width;
	}

	public Airport getAirport() {
		return this.airport;
	}

	public void setAirport(Airport airport) {
		this.airport = airport;
	}

	public Surface getSurface() {
		return this.surface;
	}

	public void setSurface(Surface surface) {
		this.surface = surface;
	}

}