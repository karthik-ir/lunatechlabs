/**
 * 
 */
package com.lunatech.airp.model.csv;

import com.opencsv.bean.CsvBindByName;

/**
 * @author karthik
 *
 */
public class CountryCSV {

	@CsvBindByName
	private Long id;
	@CsvBindByName
	private String code;
	@CsvBindByName
	private String name;
	@CsvBindByName
	private String continent;
	@CsvBindByName
	private String wikipediaLink;
	@CsvBindByName
	private String keywords;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getWikipediaLink() {
		return wikipediaLink;
	}

	public void setWikipediaLink(String wikipediaLink) {
		this.wikipediaLink = wikipediaLink;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

}
