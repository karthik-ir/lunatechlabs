/**
 * 
 */
package com.lunatech.airp.model.csv;

import com.opencsv.bean.CsvBindByPosition;

/**
 * @author karthik
 *
 */
public class RunwayCsv {
	@CsvBindByPosition(position=0)
	private Long id;
	@CsvBindByPosition(position=1)
	private Long airport_ref;
	@CsvBindByPosition(position=3)
	private Float length_ft;
	@CsvBindByPosition(position=4)
	private Float width_ft;
	@CsvBindByPosition(position=5)
	private String surface;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAirport_ref() {
		return airport_ref;
	}

	public void setAirport_ref(Long airport_ref) {
		this.airport_ref = airport_ref;
	}

	public Float getLength_ft() {
		return length_ft;
	}

	public void setLength_ft(Float length_ft) {
		this.length_ft = length_ft;
	}

	public Float getWidth_ft() {
		return width_ft;
	}

	public void setWidth_ft(Float width_ft) {
		this.width_ft = width_ft;
	}

	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

}
