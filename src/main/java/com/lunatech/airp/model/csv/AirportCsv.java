/**
 * 
 */
package com.lunatech.airp.model.csv;

import com.opencsv.bean.CsvBindByName;

/**
 * @author karthik
 *
 */
public class AirportCsv {

	@CsvBindByName
	private Long id;
	@CsvBindByName
	private String ident;
	@CsvBindByName
	private String type;
	@CsvBindByName
	private String name;
	@CsvBindByName
	private Float latitude_deg;
	@CsvBindByName
	private Float longitude_deg;
	@CsvBindByName
	private Float elevation_ft;
	@CsvBindByName
	private String continent;
	@CsvBindByName
	private String iso_country;
	@CsvBindByName
	private String wikipedia_link;
	@CsvBindByName
	private String keywords;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdent() {
		return ident;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getLatitude_deg() {
		return latitude_deg;
	}

	public void setLatitude_deg(Float latitude_deg) {
		this.latitude_deg = latitude_deg;
	}

	public Float getLongitude_deg() {
		return longitude_deg;
	}

	public void setLongitude_deg(Float longitude_deg) {
		this.longitude_deg = longitude_deg;
	}

	public Float getElevation_ft() {
		return elevation_ft;
	}

	public void setElevation_ft(Float elevation_ft) {
		this.elevation_ft = elevation_ft;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getIso_country() {
		return iso_country;
	}

	public void setIso_country(String iso_country) {
		this.iso_country = iso_country;
	}

	public String getWikipedia_link() {
		return wikipedia_link;
	}

	public void setWikipedia_link(String wikipedia_link) {
		this.wikipedia_link = wikipedia_link;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

}
