package com.lunatech.airp.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the airport database table.
 * 
 */
@Entity
@Table(name="airport")
@NamedQuery(name="Airport.findAll", query="SELECT a FROM Airport a")
public class Airport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="airport_ident")
	private String airportIdent;

	@Column(name="airport_name")
	private String airportName;

	private Float elevation;

	private String keywords;

	private Float lat;

	private Float lng;

	@Column(name="wiki_link")
	private String wikiLink;

	//bi-directional many-to-one association to Country
	@ManyToOne
	@JoinColumn(name="country_code")
	private Country country;

	//bi-directional many-to-one association to AirportType
	@ManyToOne
	@JoinColumn(name="airport_type_id")
	private AirportType airportType;

	//bi-directional many-to-one association to Runway
	@OneToMany(mappedBy="airport")
	private List<Runway> runways;

	public Airport() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAirportIdent() {
		return this.airportIdent;
	}

	public void setAirportIdent(String airportIdent) {
		this.airportIdent = airportIdent;
	}

	public String getAirportName() {
		return this.airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public float getElevation() {
		return this.elevation;
	}

	public void setElevation(Float elevation) {
		this.elevation = elevation;
	}

	public String getKeywords() {
		return this.keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Float getLat() {
		return this.lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return this.lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public String getWikiLink() {
		return this.wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public AirportType getAirportType() {
		return this.airportType;
	}

	public void setAirportType(AirportType airportType) {
		this.airportType = airportType;
	}

	public List<Runway> getRunways() {
		return this.runways;
	}

	public void setRunways(List<Runway> runways) {
		this.runways = runways;
	}

	public Runway addRunway(Runway runway) {
		getRunways().add(runway);
		runway.setAirport(this);

		return runway;
	}

	public Runway removeRunway(Runway runway) {
		getRunways().remove(runway);
		runway.setAirport(null);

		return runway;
	}

}