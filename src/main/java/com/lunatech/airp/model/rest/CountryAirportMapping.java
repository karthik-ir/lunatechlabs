package com.lunatech.airp.model.rest;

public interface CountryAirportMapping {

	public String getCountryName();

	public Integer getTotal();

}