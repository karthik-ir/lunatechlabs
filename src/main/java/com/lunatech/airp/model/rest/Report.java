/**
 * 
 */
package com.lunatech.airp.model.rest;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author karthik
 *
 */
public class Report {
	List<CountryAirportMapping> highestNumberOfAirports;
	List<CountryAirportMapping> lowestNumberOfAirports;
	Map<String, Set<String>> runwayTypes;

	public List<CountryAirportMapping> getHighestNumberOfAirports() {
		return highestNumberOfAirports;
	}

	public void setHighestNumberOfAirports(List<CountryAirportMapping> highestNumberOfAirports) {
		this.highestNumberOfAirports = highestNumberOfAirports;
	}

	public List<CountryAirportMapping> getLowestNumberOfAirports() {
		return lowestNumberOfAirports;
	}

	public void setLowestNumberOfAirports(List<CountryAirportMapping> lowestNumberOfAirports) {
		this.lowestNumberOfAirports = lowestNumberOfAirports;
	}

	public Map<String, Set<String>> getRunwayTypes() {
		return runwayTypes;
	}

	public void setRunwayTypes(Map<String, Set<String>> countriesToRunwayMapping) {
		this.runwayTypes = countriesToRunwayMapping;
	}
}