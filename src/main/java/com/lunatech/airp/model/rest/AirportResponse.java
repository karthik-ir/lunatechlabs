/**
 * 
 */
package com.lunatech.airp.model.rest;

import java.util.List;

import com.lunatech.airp.model.AirportType;

/**
 * @author karthik
 *
 */
public class AirportResponse {
	private Long id;

	private String airportIdent;

	private String airportName;

	private Float elevation;

	private String keywords;

	private Float lat;

	private Float lng;

	private String wikiLink;
	
	private AirportType airportType;

	private List<RunwayResponse> runways;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAirportIdent() {
		return airportIdent;
	}

	public void setAirportIdent(String airportIdent) {
		this.airportIdent = airportIdent;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public Float getElevation() {
		return elevation;
	}

	public void setElevation(Float elevation) {
		this.elevation = elevation;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public String getWikiLink() {
		return wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}

	public AirportType getAirportType() {
		return airportType;
	}

	public void setAirportType(AirportType airportType) {
		this.airportType = airportType;
	}

	public List<RunwayResponse> getRunways() {
		return runways;
	}

	public void setRunways(List<RunwayResponse> runways) {
		this.runways = runways;
	}

}
