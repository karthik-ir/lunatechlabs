package com.lunatech.airp.model.rest;

import java.util.List;

import com.lunatech.airp.model.Airport;
import com.lunatech.airp.model.Runway;

public interface CountryRunwayTypeMapping {

	public String getCountryName();

	public String getRunwayType();

}