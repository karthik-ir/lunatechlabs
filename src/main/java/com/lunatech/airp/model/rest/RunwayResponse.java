/**
 * 
 */
package com.lunatech.airp.model.rest;

import com.lunatech.airp.model.Surface;

/**
 * @author karthik
 *
 */
public class RunwayResponse {

	private Long id;

	private Float length;

	private Float width;

	private Surface surface;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getLength() {
		return length;
	}

	public void setLength(Float length) {
		this.length = length;
	}

	public Float getWidth() {
		return width;
	}

	public void setWidth(Float width) {
		this.width = width;
	}

	public Surface getSurface() {
		return surface;
	}

	public void setSurface(Surface surface) {
		this.surface = surface;
	}

}
