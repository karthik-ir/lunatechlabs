/**
 * 
 */
package com.lunatech.airp.model.rest;

import java.util.List;

/**
 * @author karthik
 *
 */
public class PagedAirportResponse {
	List<AirportResponse> airports;
	Long totalElements;
	int totalPages;

	public List<AirportResponse> getAirports() {
		return airports;
	}

	public void setAirports(List<AirportResponse> airports) {
		this.airports = airports;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
