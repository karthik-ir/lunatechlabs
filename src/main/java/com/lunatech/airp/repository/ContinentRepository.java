/**
 * 
 */
package com.lunatech.airp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.Continent;

/**
 * @author karthik
 *
 */
@Repository
public interface ContinentRepository extends JpaRepository<Continent, Integer> {
	Continent findByName(String name);

}
