/**
 * 
 */
package com.lunatech.airp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.Runway;

/**
 * @author karthik
 *
 */
@Repository
public interface RunwayRepository extends JpaRepository<Runway, Long> {
}
