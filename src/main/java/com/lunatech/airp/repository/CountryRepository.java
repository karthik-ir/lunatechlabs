/**
 * 
 */
package com.lunatech.airp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.Country;

/**
 * @author karthik
 *
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

	public Country findByCountryCode(String code);

}
