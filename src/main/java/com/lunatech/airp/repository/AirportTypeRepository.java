/**
 * 
 */
package com.lunatech.airp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.AirportType;

/**
 * @author karthik
 *
 */
@Repository
public interface AirportTypeRepository extends JpaRepository<AirportType, Integer> {

	public AirportType findByTypeName(String name);
}
