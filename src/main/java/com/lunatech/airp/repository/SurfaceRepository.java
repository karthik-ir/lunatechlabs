/**
 * 
 */
package com.lunatech.airp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.Surface;

/**
 * @author karthik
 *
 */
@Repository
public interface SurfaceRepository extends JpaRepository<Surface, Long> {
	public Surface findByName(String name);
}
