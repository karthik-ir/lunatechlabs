/**
 * 
 */
package com.lunatech.airp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airp.model.Airport;
import com.lunatech.airp.model.rest.CountryAirportMapping;
import com.lunatech.airp.model.rest.CountryRunwayTypeMapping;

/**
 * @author karthik
 *
 */
@Repository
public interface AirportRepository extends PagingAndSortingRepository<Airport, Long> {

	Page<Airport> findByCountryCountryNameContains(String countryName, Pageable pageable);

//	@Query("select a.country.countryName as countryName,count(s) as total from Airport a RIGHT JOIN a.country s group by a.country order by total desc")

	@Query("select a.countryName as countryName,count(s) as total from Country a LEFT JOIN a.airports s group by a order by total desc")
	List<CountryAirportMapping> findTop10AirportsDesc();

//	@Query("select a.country.countryName as countryName,count(s) as total from Airport a RIGHT JOIN a.country s group by a.country order by total asc")
	@Query("select a.countryName as countryName,count(s) as total from Country a LEFT JOIN a.airports s group by a order by total asc")
	List<CountryAirportMapping> findTop10AirportsAsc();
	
//	@Query("select x.countryName,list(runways) as runways from (select a.country.countryName, r.surface.name as runways from Airport a left join c.runways r) as x group by x.countryName")
	@Query("select a.country.countryName as countryName, r.surface.name as runwayType from Airport a left join a.runways r")
	List<CountryRunwayTypeMapping> findrunwayTypesPerCountry();
	
}


