package com.lunatech.airp;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.lunatech.airp.service.CSVProcessor;

@SpringBootApplication
public class LunatechApplication {

	public static void main(String[] args) {
		SpringApplication.run(LunatechApplication.class, args);
	}

	@Bean
	ExecutorService initExecutorService() {
		int threadCount = Runtime.getRuntime().availableProcessors();
		ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
		return threadPoolExecutor;
	}

	@Bean
	public Object processCSV(CSVProcessor csvProcessor, @Value("${csv.country}") File country,
			@Value("${csv.runway}") File runway, @Value("${csv.airport}") File airport)
			throws InterruptedException, ExecutionException {
		return csvProcessor.init(country, airport, runway);
	}
}
