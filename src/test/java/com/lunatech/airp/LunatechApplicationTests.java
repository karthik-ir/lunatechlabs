package com.lunatech.airp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lunatech.airp.model.Airport;
import com.lunatech.airp.model.rest.AirportResponse;
import com.lunatech.airp.model.rest.Report;
import com.lunatech.airp.repository.AirportRepository;
import com.lunatech.airp.repository.AirportTypeRepository;
import com.lunatech.airp.repository.ContinentRepository;
import com.lunatech.airp.repository.CountryRepository;
import com.lunatech.airp.repository.RunwayRepository;
import com.lunatech.airp.repository.SurfaceRepository;
import com.lunatech.airp.service.AirpService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LunatechApplicationTests {

	@Autowired
	private ContinentRepository continentRepository;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private AirportRepository airportRepository;
	@Autowired
	private AirportTypeRepository airportTypeRepository;
	@Autowired
	private SurfaceRepository surfaceRepository;
	@Autowired
	private RunwayRepository runwayRepository;

	@Autowired
	AirpService airpService;

	//Sample test to check if the DB is being populated
	@Test
	public void testRepo() {
		assertEquals(247, countryRepository.count());
	}

	// Test Query with variations
	@Test
	public void testQueryByCountryName() {
		List<AirportResponse> airportsByCountry = airpService.getAirportsByCountry("United");
		assertEquals(79, airportsByCountry.size());

		airportsByCountry = airpService.getAirportsByCountry("Br");
		assertEquals(462, airportsByCountry.size());
		
		airportsByCountry = airpService.getAirportsByCountry("Blah");
		assertEquals(0, airportsByCountry.size());
	}

	//Test report
	@Test
	public void testReport() {
		Report report = airpService.getReport();
		assertTrue(report.getHighestNumberOfAirports().get(0).getCountryName().equals("Colombia"));
		assertFalse(report.getLowestNumberOfAirports().get(0).getCountryName().equals("United States"));
		assertEquals(report.getRunwayTypes().get("United States").size(),10);
		
		assertTrue(report.getHighestNumberOfAirports().size()==10);
		assertTrue(report.getLowestNumberOfAirports().size()==10);

	}

}
